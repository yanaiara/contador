document.getElementById("countButton").onclick = function () {
    let typedText = document.getElementById("textInput").value;       // CRIAR O CLICK

    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");   //substitui os caracteres especiais

    const letterCounts = {};    //declarando uma constante de objeto vazio

    for (let i = 0; i < typedText.length; i++) {     // percorre todas as letras do texto   
        currentLetter = typedText[i];
        if (currentLetter !== " ") {
            if (letterCounts[currentLetter] === undefined) {
                letterCounts[currentLetter] = 1;
            } else {
                letterCounts[currentLetter]++;
            }
        }
    }

    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }

    const wordsCounts = {};
    let words = typedText.split(/\s/);


    for (let i = 0; i < words.length; i++) {     // percorre todas as palavras do texto   

        let currentwords = words[i];
        if (currentwords !== "") {
            if (wordsCounts[currentwords] === undefined) {
                wordsCounts[currentwords] = 1;
            } else {
                wordsCounts[currentwords]++;
            }
        }
    }
    console.log(wordsCounts);

    for (let words in wordsCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + words + "\": " + wordsCounts[words] + ", ");
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
    }




}